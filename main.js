//Потому что на современных устройствах есть и другие способы «ввести что-то».
// Например, распознавание речи или Копировать/Вставить с помощью мыши.
// Поэтому, если мы хотим корректно отслеживать ввод в поле <input>,
// то одних клавиатурных событий недостаточно. Существует специальное событие input,
// чтобы отслеживать любые изменения в поле <input>

const btn = document.getElementsByTagName('button');
document.addEventListener('keyup', toKeyBackLight);
btn[0].value = 'Enter';
btn[1].value = 'KeyS';
btn[2].value = 'KeyE';
btn[3].value = 'KeyO';
btn[4].value = 'KeyN';
btn[5].value = 'KeyL';
btn[6].value = 'KeyN';

function toKeyBackLight(event){
    for (let i = 0; i < btn.length; i++) {
        btn[i].style.backgroundColor = "#33333a";
        if (btn[i].value === event.code) {
            btn[i].style.backgroundColor = 'blue';
        }
    }
}